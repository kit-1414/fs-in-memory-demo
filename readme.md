 It's required to implement light file system "in memory".

 FS must provide the API:

  - create file
  - read file
  - copy/move file
  - edit file (content edit)
  - delete file

  There are 2 file types allowed : file && folder.

  There are unit tests required.

  -------------------------------------
  Implentation notes:

   - FS implementation is not thread-save.
   - files and folder have names like "/name1/name2/nameN"
   - root folder has name "/"
   - file/ folder names can contain chars: [a-z] [A-Z] [0-9] '_'  '-' '.'
   - file/ folder names are  CaseSensitive: "/file" "/fIle"  are different files. 
   - size of folder means child number.
   - if some folder or file need to be create, copy, move - dest folder must exist.
   - it's impossible to change file size after it created. if you need it, just delete and create again.

---------------------------
  Ways to improve FS (with same API logic):

 - create new interface for FS which uses FsPath  instead of String as file and folder name
   It's need to remain interface version with String as names , but it's required to implement it as proxy over FsPath version.
   Unit test based on String version and cover all code.
 - to implement "move" operation via real node moving. Not it implemented via "copy and delete old"
 - to implement  function FsInMemory::appendFile(String fileName, byte[] data) to increate it's size withour recreaing.
 - to implement thread-safe logic (by Serialisable lock).
 - to implement thread-safe logic by logic : many readers and one writer.
       there are 2 levels of lock: 
       1st lock tree structure (while search, create, delete nodes in progress), 
       then rlease tree and lock node (file) self (if need) for read-write data operation.
 - to implement file data storing not by solid array, but as list of arrays.
 - to add max limit for file data size
 - to add max limit for folders tree depth
 - to add max limit for single file/folder name length
 - to write javadocs for all files.