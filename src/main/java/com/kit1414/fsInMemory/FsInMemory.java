package com.kit1414.fsInMemory;

import java.util.Set;

public interface FsInMemory {
    String PATH_SEPARATOR = "/";
    String ROOT_FOLDER_NAME = PATH_SEPARATOR;

    String createFile(String fileName, byte[] data); // return full file name
    String createFolder(String folderName); // return full folder name

    int readFile(String fileName, int offset, byte[] data); // return read bytes number
    Set<String> readFolder(String folderName); // return names of folder child

    int editFile(String fileName,int offset, byte[] dataToWrite);

    boolean isExists(String fsName); // for file and folder both
    boolean isFile(String fsName);// for file and folder both
    boolean isFolder(String fsName); // for file and folder both
    int getSize(String fsName); // for file and folder both

    String copy(String fsSrcFileName, String fsDestFileName); // return new path
    String move(String fsSrcName, String fsDestName);// return new path
    boolean delete(String fsName); //recursive delete file/folder

}
