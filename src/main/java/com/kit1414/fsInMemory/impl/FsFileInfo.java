package com.kit1414.fsInMemory.impl;

/**
 * Created by kit_1414 on 04-Dec-16.
 */
public class FsFileInfo {
    private int size;
    private boolean isFolder;
    private FsPath path;

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public boolean isFolder() {
        return isFolder;
    }

    public void setFolder(boolean folder) {
        isFolder = folder;
    }

    public boolean isFile() {
        return !isFolder();
    }

    public FsPath getPath() {
        return path;
    }

    public void setPath(FsPath path) {
        this.path = path;
    }

    public FsFileInfo getClone() {
        FsFileInfo newInfo = new FsFileInfo();
        newInfo.setPath(path.getClone());
        newInfo.setFolder(isFolder());
        newInfo.setSize(getSize());
        return newInfo;
    }

    @Override
    public String toString() {
        return "FsFileInfo{" +
                "size=" + size +
                ", isFolder=" + isFolder +
                ", path=" + path.buildFullName() +
                '}';
    }
}
