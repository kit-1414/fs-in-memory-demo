package com.kit1414.fsInMemory.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by aamelin on 02.12.2016.
 */
public abstract class FsPrototype implements Comparable <FsPrototype> {

    private String name;
    private FsFolder parent;

    abstract public boolean isFile();
    abstract int  getSize();
    abstract FsPrototype buildCopy();

    public FsPrototype() {

    }
    public FsPrototype(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FsFolder getParent() {
        return parent;
    }

    public void setParent(FsFolder parent) {
        this.parent = parent;
    }

    public String buildFullName() {
        List<String> list = new ArrayList<>();
        FsPrototype fs = this;
        while(fs.parent != null) {
            list.add(fs.getName());
            fs = fs.parent;
        }
        Collections.reverse(list);
        return FsUtils.buildFullName(list);
    }
    public boolean isFolder() {
        return !isFile();
    }

    @Override
    public int compareTo(FsPrototype o) {
        if (isFile() != o.isFile()) {
            return isFile()? -1 : 1;
        }
        return name.compareTo(o.name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FsPrototype that = (FsPrototype) o;

        return name != null ? name.equals(that.name) : that.name == null;

    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "FsPrototype{" + "name='" + name + '\'' + ", isFile=" + isFile() + ", fullName="+ buildFullName() + '}';
    }
}

