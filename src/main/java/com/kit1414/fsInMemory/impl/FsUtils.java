package com.kit1414.fsInMemory.impl;

import com.kit1414.fsInMemory.FsInMemory;
import com.kit1414.fsInMemory.exception.FsException;
import com.kit1414.fsInMemory.exception.FsInvalidFileNameException;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by aamelin on 02.12.2016.
 */
public class FsUtils {
    private static final String PATH_SEPARATOR = FsInMemory.PATH_SEPARATOR;

    private static final String SIMPLE_NAME_REGEX = "([a-zA-Z0-9\\-_\\.]+)";
    private static final String FULL_NAME_REGEX = String.format("^%s|(%s%s)+$", PATH_SEPARATOR, PATH_SEPARATOR, SIMPLE_NAME_REGEX);

    private static final Pattern SIMPLE_NAME_PATTERN = Pattern.compile(SIMPLE_NAME_REGEX);
    private static final Pattern FULL_NAME_PATTERN = Pattern.compile(FULL_NAME_REGEX);

    public static boolean isShortFsNameValid(String fileName) {
        return fileName!= null && SIMPLE_NAME_PATTERN.matcher(fileName).matches();
    }

    public static String validateShortFileName(String fileName) {
        if (isShortFsNameValid(fileName)) {
            return fileName;
        }
        throw new FsInvalidFileNameException(fileName);
    }

    public static boolean isFullFsNameValid(String fileName) {
        return PATH_SEPARATOR.equals(fileName) || (fileName!= null && FULL_NAME_PATTERN.matcher(fileName).matches());
    }

    public static String validateFullFileName(String fileName) {
        if (isFullFsNameValid(fileName)) {
            return fileName;
        }
        throw new FsInvalidFileNameException(fileName);
    }

    public static String buildFullName(List<String> path) {
        if (path.size() > 0) {
            StringBuilder sb = new StringBuilder();
            for (String s : path) {
                sb.append(PATH_SEPARATOR).append(s);
            }
            return sb.toString();
        }
        return PATH_SEPARATOR;
    }

    public static List<String> splitFileNameToList(String fileName) {
        List<String> result  = Arrays.asList(fileName.split(PATH_SEPARATOR)).stream().filter(it -> it!= null && it.trim().length() >0).collect(Collectors.toList());
        return new ArrayList<>(result);
    }

    public static String readFileToString(FsInMemory fs, String fileName) {
        byte[] data = readFileAsBytes(fs,fileName);
        return data == null ? null : new String(data);
    }
    public static byte[] readFileAsBytes(FsInMemory fs, String fileName) {
        byte[] result = null;
        if (fs.isExists(fileName) && fs.isFile(fileName)) {
            int dataSize = fs.getSize(fileName);
            result = new byte[dataSize];
            fs.readFile(fileName,0,result);
        }
        return result;
    }

    public static String checkCreateFile(FsInMemory fs, FsPath fileToCreate, byte[] fileContent) {
        FsPath parent = fileToCreate.getParent();
        checkCreateFolder(fs,parent);
        return fs.createFile(fileToCreate.buildFullName(), fileContent);
    }

    public static String checkCreateFolder(FsInMemory fs, FsPath folderToCreate) {
        String fullName = folderToCreate.buildFullName();
        if (fs.isExists(fullName) && fs.isFolder(fullName)) {
            return fullName;
        }
        FsPath checkPath = new FsPath();
        for (String subNames : folderToCreate.getPathAsList()) {
            checkPath.addOne(subNames);
            String subPath = checkPath.buildFullName();
            if (fs.isExists(subPath )) {
                if (fs.isFile(subPath)) {
                    throw new FsException(String.format("Can't create folder [%s], file [%s] already exists", fullName, subPath));
                }
            } else {
                fs.createFolder(subPath);
                //fs.isFolder(subPath);
            }

        }
        return checkPath.buildFullName();
    }


    public static List<FsFileInfo> scanAllContent(FsInMemory fs, String basePath) {
        ArrayList<FsFileInfo> result = new ArrayList<>();
        FsFileInfo fiRoot = createFileInfo(fs,basePath);
        scanAllContentRecursive(fs, fiRoot, result);
        return result;
    }

    private static List<FsFileInfo> scanAllContentRecursive(FsInMemory fs, FsFileInfo fileInfo, List<FsFileInfo> result) {
        result.add(fileInfo);
        FsPath path = fileInfo.getPath();
        String fullName  = path.buildFullName();
        if (fileInfo.isFolder()) {
            Set<String> child = fs.readFolder(fullName);
            for (String childSubName : child) {
                FsPath childPath = path.getClone().addOne(childSubName);
                FsFileInfo fiChild = createFileInfo(fs,childPath.buildFullName());
                if (fiChild != null) {
                    scanAllContentRecursive(fs, fiChild, result);
                }
            }
        }
        return result;
    }
    public static FsFileInfo createFileInfo(FsInMemory fs, String path) {
        FsFileInfo result = null;
        if (fs.isExists(path)) {
            result = new FsFileInfo();
            result.setPath(new FsPath(path));
            result.setFolder(fs.isFolder(path));
            result.setSize(fs.getSize(path));
        }
        return result;
    }
}
