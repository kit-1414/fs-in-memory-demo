package com.kit1414.fsInMemory.impl;

/**
 * Created by kit_1414 on 04-Dec-16.
 */
public class FsCopyInfo {
    private FsFileInfo oldPath;
    private FsFileInfo newPath;


    public FsFileInfo getOldPath() {
        return oldPath;
    }

    public void setOldPath(FsFileInfo oldPath) {
        this.oldPath = oldPath;
    }

    public FsFileInfo getNewPath() {
        return newPath;
    }

    public void setNewPath(FsFileInfo newPath) {
        this.newPath = newPath;
    }

    public boolean isFolder() {
        return oldPath.isFolder();
    }
    public boolean isFile() {
        return !isFolder();
    }

}
