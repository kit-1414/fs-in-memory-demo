package com.kit1414.fsInMemory.impl;

import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * Created by aamelin on 02.12.2016.
 */
public class FsFile extends FsPrototype {
    private byte [] content;

    public FsFile(FsFolder parent,String name, byte[] content) {
        super(name);
        this.content = Arrays.copyOf(content,content.length);
        setParent(parent);
    }

    @Override
    public boolean isFile() {
        return true;
    }

    @Override
    int getSize() {
        return content.length;
    }

    public FsFile buildCopy() {
        return new FsFile(null, getName(), content);
    }
    public int editFile(int offset, byte[] dataToWrite) {
        if (dataToWrite == null || dataToWrite.length == 0) {
            return 0;
        }
        int dataToWritePossible = content.length - offset;
        if (dataToWritePossible <= 0) {
            return 0;
        }
        int dataSize = Math.min(dataToWrite.length,dataToWritePossible);
        if (dataSize > 0) {
            System.arraycopy(dataToWrite,0,content,offset,dataSize);
        }
        return dataSize;
    }
    public int readFile(int offset, byte[] data) {
        if (offset < 0) {
            return 0;
        }
        int dataReadPossible = content.length - offset;
        if (dataReadPossible <=0) {
            return 0;
        }
        int readData = Math.min(dataReadPossible,data.length);
        System.arraycopy(content,offset,data,0,readData);
        return readData;
    }
}
