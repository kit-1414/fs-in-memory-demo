package com.kit1414.fsInMemory.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import com.kit1414.fsInMemory.exception.FsDuplicateNameException;

/**
 * Created by aamelin on 02.12.2016.
 */
public class FsFolder extends FsPrototype {
    private final FsFolder parentFolder;
    private HashMap<String,FsPrototype> content = new HashMap<>();

    public FsFolder(FsFolder parentFolder, String name) {
        this.parentFolder = parentFolder;
        this.setName(name);
    }

    public FsFolder getParentFolder() {
        return parentFolder;
    }

    public boolean isChildExists(String shotName) {
        return content.keySet().contains(shotName);
    }
    public boolean isChildExists(FsPrototype file) {
        return isChildExists(file.getName());
    }
    public FsPrototype findChild(String fsShortName) {
        return content.get(fsShortName);
    }
    public boolean removeChild(FsPrototype file) {
        return removeChild(file.getName());
    }
    public boolean removeChild(String name) {
        FsPrototype toRemove  = content.remove(name);
        if (toRemove != null) {
            toRemove.setParent(null); // for sure
        }
        return toRemove != null;
    }
    public String addChild(FsPrototype file) {
        if (isChildExists(file.getName())) {
            FsPath path = new FsPath(this.buildFullName());
            path.addOne(file.getName());
            throw new FsDuplicateNameException(path.buildFullName());
        }
        content.put(file.getName(),file);
        file.setParent(this);
        return file.buildFullName();
    }
    public Set<String> getChildNames() {
        return content.values().stream().map(FsPrototype::getName).collect(Collectors.toSet());
    }

    @Override
    public boolean isFile() {
        return false;
    }

    @Override
    int getSize() {
        return content.size();
    }

    @Override
    public FsFolder buildCopy() {
        FsFolder folder =  new FsFolder(null,getName());
        for (FsPrototype p : content.values()) {
            folder.addChild(p.buildCopy()); // full recursive
        }
        return folder;
    }

}
