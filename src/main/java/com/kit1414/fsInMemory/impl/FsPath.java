package com.kit1414.fsInMemory.impl;


import com.kit1414.fsInMemory.exception.FsDuplicateNameException;
import com.kit1414.fsInMemory.exception.FsInvalidPathException;
import com.kit1414.fsInMemory.exception.FsParentNotFoundException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kit_1414 on 03-Dec-16.
 */
public class FsPath {

    private final ArrayList<String> path = new ArrayList<>();

    public FsPath() {
    }

    public FsPath(String fsName) {
        path.addAll(FsUtils.splitFileNameToList(fsName));
    }
    public FsPath(List<String> path) {
        this.path.addAll(path);
    }
    public FsPath(FsPath path) {
        this(path.path);
    }

    public List<String> getPathAsList() {
        return new ArrayList<>(path);
    }
    public String []getPathAsArray() {
        return path.toArray(new String[path.size()]);
    }
    public int getSize() {
        return path.size();
    }

    public boolean isRoot() {
        return getSize() == 0;
    }
    public FsPath getParent() {
        if (isRoot()) {
            return null;
        }
        return new FsPath(path.subList(0,path.size()-1));
    }
    public String buildFullName() {
        return FsUtils.buildFullName(path);
    }

    public FsPath addOne(String name) {
        path.add(name);
        return this;
    }

    public String getFirst() {
        return isRoot() ? null : path.iterator().next();
    }
    public String getLast() {
        return isRoot() ? null : path.get(path.size() - 1);
    }

    @Override
    public String toString() {
        return "FsPath {" +
                    "path=" + buildFullName() +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || getClass()!= obj.getClass()) {
            return false;
        }
        FsPath another = (FsPath) obj;
        return buildFullName().equals(another.buildFullName());
    }
    public static FsPath createNew(String path) {
        return new FsPath(path);
    }
    public static FsPath createNew(FsPath path) {
        return new FsPath(path);
    }
    public FsPath getClone() {
        FsPath newPath = new FsPath();
        newPath.path.addAll(path);
        return newPath;
    }
}

