package com.kit1414.fsInMemory.impl;

import com.kit1414.fsInMemory.exception.*;

import com.kit1414.fsInMemory.FsInMemory;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class FsInMemoryImpl implements FsInMemory {
    private final static Logger log = Logger.getLogger(FsInMemoryImpl.class);

    private FsFolder rootFolder = new FsFolder(null, null);

    @Override
    public String createFile(String fileName, byte[] data) {
        log.debug(String.format("fileName = [%s], data size=%d", fileName, data.length));

        validateFullName(fileName);
        FsFolder parent = getParentToCreate(fileName);
        FsPath fsPath = new FsPath(fileName);
        FsFile file = new FsFile(parent, fsPath.getLast(), data);
        parent.addChild(file);
        return file.buildFullName();
    }

    @Override
    public String createFolder(String folderName) {
        log.debug(String.format("folderName = [%s]", folderName));
        validateFullName(folderName);
        FsFolder parent = getParentToCreate(folderName);
        FsPath fsPath = new FsPath(folderName);
        FsFolder folder = new FsFolder(parent, fsPath.getLast());
        parent.addChild(folder);
        return folder.buildFullName();
    }

    @Override
    public int readFile(String fileName, int offset, byte[] data) {
        log.debug(String.format("fileName = [%s], offset=%d, data size=%d", fileName, offset, data.length));
        validateFullName(fileName);
        FsFile file = checkFileByPath(fileName);
        return file.readFile(offset, data);
    }

    @Override
    public Set<String> readFolder(String folderName) {
        log.debug(String.format("folderName = [%s]", folderName));
        validateFullName(folderName);
        FsFolder folder = checkFolderByPath(folderName);
        return folder.getChildNames();
    }

    @Override
    public int editFile(String fileName, int offset, byte[] data) {
        log.debug(String.format("fileName = [%s], offset=%d, data size=%d", fileName, offset, data.length));
        validateFullName(fileName);
        FsFile file = checkFileByPath(fileName);
        return file.editFile(offset, data);
    }

    @Override
    public boolean isExists(String fsName) {
        log.debug(String.format("fsName = [%s]", fsName));
        validateFullName(fsName);
        return findByPath(fsName) != null;
    }

    @Override
    public boolean isFile(String fsName) {
        log.debug(String.format("fsName = [%s]", fsName));
        validateFullName(fsName);
        FsPrototype proto = checkByPath(fsName);
        return proto.isFile();
    }

    @Override
    public boolean isFolder(String fsName) {
        log.debug(String.format("fsName = [%s]", fsName));
        validateFullName(fsName);
        FsPrototype proto = checkByPath(fsName);
        return proto.isFolder();
    }

    @Override
    public int getSize(String fsName) {
        log.debug(String.format("fsName = [%s]", fsName));
        validateFullName(fsName);
        return checkByPath(fsName).getSize();
    }

    @Override
    public String copy(String fsSrcName, String fsDestFolderName) {
        log.debug(String.format("fsSrcName = [%s], fsDestFolderName=[%s]", fsSrcName, fsDestFolderName));
        copyAll(fsSrcName, fsDestFolderName);
        return getNewNameForMoveCopy(fsSrcName,fsDestFolderName);
    }

    @Override
    public String move(String fsSrcName, String fsDestFolderName) {
        log.debug(String.format("fsSrcName = [%s], fsDestFolderName=[%s]", fsSrcName, fsDestFolderName));
        List<FsCopyInfo> filesList = copyAll(fsSrcName, fsDestFolderName);
        for (FsCopyInfo info : filesList) {
            delete(info.getOldPath().getPath().buildFullName());
        }
        return getNewNameForMoveCopy(fsSrcName,fsDestFolderName);
    }

    @Override
    public boolean delete(String fsName) {
        validateFullName(fsName);
        FsPrototype proto = findByPath(fsName);
        if (proto == null) {
            return false;
        }
        if (rootFolder == proto) {
            throw new FsException("root folder can't be deleted: [" + fsName + "]");
        }
        FsFolder parent = proto.getParent();
        return parent.removeChild(proto.getName());
    }

    private String getNewNameForMoveCopy(String srcPath, String destPath) {
        FsPath fsSrcPath = new FsPath(srcPath);
        return destPath + FsInMemory.PATH_SEPARATOR + fsSrcPath.getLast();
    }
    private List<FsCopyInfo> copyAll(String fsSrcName, String fsDestFolderName) {
        FsPrototype protoSrc = checkByPath(fsSrcName);
        checkFolderByPath(fsDestFolderName);
        if (rootFolder == protoSrc) {
            throw new FsException("Root folder can't be copied, fsSrcFileName=[" + fsSrcName + "]");
        }
        String newName = getNewNameForMoveCopy(fsSrcName, fsDestFolderName);
        if (fsSrcName.equals( newName )) {
            throw new FsException("it's not allowed move/copy to itself fs name = [" + fsSrcName + "]");
        }
        // create list of new files and folders, thant must exist in dest folder with it's new full names
        List<FsCopyInfo> result = new ArrayList<>();

        List<FsFileInfo> filesToCopyMove  = FsUtils.scanAllContent(this,fsSrcName);
        for (FsFileInfo fi: filesToCopyMove ) {
            FsCopyInfo copyInfo = new FsCopyInfo();
            copyInfo.setOldPath(fi);
            result.add(copyInfo);

            String fullName = fi.getPath().buildFullName();
            String newFsName = fullName.substring(fsSrcName.length()- protoSrc.getName().length()); // remove all path from start till root of copy.
            newFsName = fsDestFolderName + FsInMemory.PATH_SEPARATOR + newFsName;

            FsFileInfo newInfo = fi.getClone();
            newInfo.setPath(new FsPath(newFsName));
            copyInfo.setNewPath(newInfo);
        }
        // check there are no files with same names as new or subfolder for new fsItem.
        for (FsCopyInfo fci : result) {
            FsPath newPath = fci.getNewPath().getPath();
            FsPath fullPathToCheck = newPath.getParent();
            FsPath pathToCheck = new FsPath();
            for (String name : fullPathToCheck.getPathAsList()) {
                pathToCheck.addOne(name);
                FsFileInfo fileInfo = FsUtils.createFileInfo(this,pathToCheck.buildFullName());
                if (fileInfo != null && fileInfo.isFile()) {
                    throw new FsDuplicateNameException(fileInfo.getPath().buildFullName()," while move/copy try to replace this file by folder.");
                }
            }
            // if new FsItem exist by path, then it must be same type as old.
            FsFileInfo currentFileInfo = FsUtils.createFileInfo(this,newPath.buildFullName());
            if (currentFileInfo != null && currentFileInfo.isFile() != fci.isFile()) {
                String endErrMsg = currentFileInfo.isFile()? " this file by folder." : " this folder by file." ;
                throw new FsDuplicateNameException(currentFileInfo.getPath().buildFullName()," while move/copy try to replace " + endErrMsg);
            }
        }


        // create files and folders in destFolder
        // TODO: it's need to improve: implement real FSPrototype node moving for "move" mode. It "copy" now for all cases.
        for (FsCopyInfo ci : result) {
            if (ci.isFolder()) {
                // for folders, we should check (and create, if need) all new folders
                FsUtils.checkCreateFolder(this,ci.getNewPath().getPath());
            } else {
                // for files, we should create (move : need implement) (and delete old, if need) all files
                String newPath = ci.getNewPath().getPath().buildFullName();
                byte[] fileData = FsUtils.readFileAsBytes(this,ci.getOldPath().getPath().buildFullName());
                if (isExists(newPath)) {
                    // if file exists, delete file
                    delete(newPath);
                }
                createFile(newPath,fileData);
            }
        }
        return result;
    }

    private FsPrototype findByPath(String fullName) {
        return findByPath(new FsPath(fullName));
    }

    private FsPrototype findByPath(FsPath path) {
        FsFolder folder = rootFolder;
        FsPrototype proto = folder;
        for (String s : path.getPathAsList()) {
            if (folder == null) {
                return null;
            } else {
                proto = folder.findChild(s);
                if (proto == null) {
                    return null;
                }
                if (proto instanceof FsFolder) {
                    folder = (FsFolder) proto;
                }
            }
        }
        return proto;
    }

    private FsPrototype checkByPath(String fileName) {
        FsPrototype proto = findByPath(fileName);
        if (proto != null) {
            return proto;
        }
        throw new FsFileNotFoundException(fileName);
    }

    private FsFile findFileByPath(String fileName) {
        FsPrototype prototype = findByPath(fileName);
        return prototype != null && prototype.isFile() ? (FsFile) prototype : null;
    }

    private FsFolder findFolderByPath(String folderName) {
        FsPrototype prototype = findByPath(folderName);
        return prototype != null && prototype.isFolder() ? (FsFolder) prototype : null;
    }

    private FsFile checkFileByPath(String fileName) {
        FsFile file = findFileByPath(fileName);
        if (file != null) {
            return file;
        }
        throw new FsFileNotFoundException(fileName);
    }

    private FsFolder checkFolderByPath(String folderName) {
        FsFolder folder = findFolderByPath(folderName);
        if (folder != null) {
            return folder;
        }
        throw new FsFolderNotFoundException(folderName);
    }

    private String validateFullName(String fileName) {
        return FsUtils.validateFullFileName(fileName);
    }

    public FsFolder findParentFolder(FsPath path) {
        if (path.isRoot()) {
            throw new FsInvalidPathException("can't find parent folder for [" + path.toString() + "]");
        }
        FsPrototype proto = findByPath(path.getParent());
        if (proto != null && proto.isFolder()) {
            return (FsFolder) proto;
        }
        throw new FsParentNotFoundException(path.toString());
    }

    public FsFolder checkParentFolder(FsPath path) {
        FsFolder parent = findParentFolder(path);
        if (parent != null) {
            return parent;
        }
        throw new FsParentNotFoundException(path.toString());


    }

    private FsFolder getParentToCreate(String fileName) {
        return checkParentFolder(new FsPath(fileName));
    }

}
