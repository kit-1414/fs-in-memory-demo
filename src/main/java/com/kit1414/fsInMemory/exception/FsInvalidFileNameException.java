package com.kit1414.fsInMemory.exception;

/**
 * Created by aamelin on 02.12.2016.
 */
public class FsInvalidFileNameException extends  FsException {
    public FsInvalidFileNameException(String wrongFileName) {
        super("File/folder name is not valid: [" + wrongFileName + "]");
    }
}
