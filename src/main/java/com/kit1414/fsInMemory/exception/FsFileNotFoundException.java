package com.kit1414.fsInMemory.exception;

/**
 * Created by aamelin on 02.12.2016.
 */
public class FsFileNotFoundException extends FsException {
    public FsFileNotFoundException(String fileName) {
        super("File not found: [" + fileName+"]");
    }
}
