package com.kit1414.fsInMemory.exception;

/**
 * Created by kit_1414 on 03-Dec-16.
 */
public class FsInvalidPathException extends FsException {
    public FsInvalidPathException(String reason) {
        super(reason);
    }
}
