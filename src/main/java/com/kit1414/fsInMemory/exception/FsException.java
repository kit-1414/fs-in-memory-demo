package com.kit1414.fsInMemory.exception;

/**
 * Created by aamelin on 02.12.2016.
 */
public class FsException extends RuntimeException {
    public FsException(String reason) {
        super(reason);
    }
}
