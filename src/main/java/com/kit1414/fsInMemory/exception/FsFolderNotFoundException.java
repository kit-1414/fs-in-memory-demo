package com.kit1414.fsInMemory.exception;

/**
 * Created by kit_1414 on 03-Dec-16.
 */
public class FsFolderNotFoundException extends FsException {
    public FsFolderNotFoundException(String folderName) {
        super("Folder not found: [" + folderName+"]");
    }
}
