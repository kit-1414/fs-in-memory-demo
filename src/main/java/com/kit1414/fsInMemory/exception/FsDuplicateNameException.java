package com.kit1414.fsInMemory.exception;

/**
 * Created by aamelin on 02.12.2016.
 */
public class FsDuplicateNameException extends FsException {
    public FsDuplicateNameException(String newName) {
        this(newName,null);
    }
    public FsDuplicateNameException(String newName, String reason) {
        super("File/Folder already exists: [" + newName + "]"  + (reason == null? "" : ", "+reason));
    }
}
