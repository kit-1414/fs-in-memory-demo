package com.kit1414.fsInMemory.exception;

/**
 * Created by kit_1414 on 03-Dec-16.
 */
public class FsParentNotFoundException extends FsException {
    public FsParentNotFoundException(String fileName) {
        super("parent not exist or not a folder for path [" + fileName + "]");
    }
}
