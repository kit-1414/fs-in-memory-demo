package com.kit1414.fsInMemory.exception;

/**
 * Created by kit_1414 on 03-Dec-16.
 */
public class FsPathNotFoundException extends FsException {
    public FsPathNotFoundException(String path) {
        super("path not exists " + path + "]");
    }
}
