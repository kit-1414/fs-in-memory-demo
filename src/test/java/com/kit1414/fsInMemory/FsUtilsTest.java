package com.kit1414.fsInMemory;

import com.kit1414.fsInMemory.impl.FsUtils;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by kit_1414 on 03-Dec-16.
 */
public class FsUtilsTest {

    private static final String [] VALID_FULL_NAMES = {
            "/",
            "/aaa",
            "/aaa123456789",
            "/aaa123456789/09786",
            "/aaa123456789/09786/aa",
            "/aaa123456789/09786/aa/d",
            "/aaa123456789/09786/aa_/d",
            "/aaa123456789/09786/aa_kk/d",
            "/aaa123456789/09786/aa_kk-",
            "/aaa123456789/09786/aa_kk-/d"
    };

    private static final String [] INVALID_FULL_NAMES = {
            "/a/",
            "//a",
            "a//a",
            "/a//a",
            "//a//",
            "/a//",
            "/acdc/dddd//zzz",
            "/acdc//dddd/zzz",
            "//acdc/dddd/zzz",
            "/acdc/dddd/zzz/"
    };
    private static final String [] VALID_SHOT_NAMES = {
            "aaa0123456789-_.",
            "aaa",
            "aaa",
            "09786",
            ".09786.-_",
            "someFile.png",
            "someFileAAZZ123456789_a-.png",
            "someFile.log_"
    };
    private static final String [] INVALID_SHOT_NAMES= {
            "",
            "$acd",
            "1234*",
            "a|s"
    };

    @Test
    public void validShortNameTest() {
        for (String name : VALID_SHOT_NAMES) {
            Assert.assertTrue(FsUtils.isShortFsNameValid(name));
        }
    }
    @Test
    public void invalidShortNameTest() {
        for (String name: INVALID_SHOT_NAMES) {
            Assert.assertFalse(FsUtils.isShortFsNameValid(name));
        }
    }

    @Test
    public void validFullNameTest() {
        for (String name : VALID_FULL_NAMES) {
            Assert.assertTrue(FsUtils.isFullFsNameValid(name));
        }
    }
    @Test
    public void invalidFullNameTest() {
        for (String name: INVALID_FULL_NAMES) {
            Assert.assertFalse(FsUtils.isFullFsNameValid(name));
        }
    }
}
