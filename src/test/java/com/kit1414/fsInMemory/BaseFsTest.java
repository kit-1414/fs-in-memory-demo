package com.kit1414.fsInMemory;

import com.kit1414.fsInMemory.exception.FsDuplicateNameException;
import com.kit1414.fsInMemory.impl.FsFileInfo;
import com.kit1414.fsInMemory.impl.FsInMemoryImpl;
import com.kit1414.fsInMemory.impl.FsPath;
import com.kit1414.fsInMemory.impl.FsUtils;
import org.apache.log4j.Logger;
import org.junit.Assert;


import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by kit_1414 on 03-Dec-16.
 */
public class BaseFsTest {

    private final static Logger log = Logger.getLogger(BaseFsTest.class);

    protected FsInMemory fs;

    public void setUp() {
        fs = new FsInMemoryImpl();
    }


    protected static void checkCreateCreateFolder(FsInMemory fs, FsPath fsRoot, FsPath fsNewFolder) {
        final String rootFolder = fsRoot.buildFullName();
        final String folder1 = fsNewFolder.buildFullName();

        int oldBaseRootSize = fs.getSize(rootFolder);
        Assert.assertFalse(fs.isExists(folder1));
        String f1 = fs.createFolder(folder1);
        Assert.assertTrue(fs.isExists(folder1));
        Assert.assertTrue(fs.isFolder(folder1));
        Assert.assertFalse(fs.isFile(folder1));
        Assert.assertEquals(f1, folder1);
        Assert.assertEquals(oldBaseRootSize+1, fs.getSize(rootFolder));


        try {
            fs.createFolder(folder1);
            Assert.fail(folder1 + " already exists");
        } catch (FsDuplicateNameException ex) {
            // Nothing
        }
    }
    protected static void checkCreateCreateFile(FsInMemory fs, FsPath fsRoot, FsPath fsNewFile, String fileContent) {
        final String file1= fsNewFile.buildFullName();
        final String rootFolder = fsRoot.buildFullName();

        int oldBaseRootSize = fs.getSize(rootFolder);
        Assert.assertFalse(fs.isExists(file1));
        String f3 = fs.createFile(file1, fileContent.getBytes());
        Assert.assertEquals(oldBaseRootSize+1, fs.getSize(rootFolder));

        Assert.assertEquals(f3, file1);
        Assert.assertTrue(fs.isExists(file1));
        Assert.assertFalse(fs.isFolder(file1));
        Assert.assertTrue(fs.isFile(file1));
        checkFileContent(fs,new FsPath(file1),fileContent);

        try {
            fs.createFolder(file1);
            Assert.fail(file1 + " already exists");
        } catch (FsDuplicateNameException ex) {
            // Nothing
        }

    }

    protected static void checkFileContent(FsInMemory fs, FsPath fsFile, String baseContent) {
        final String fileContent = baseContent;
        final String fileName = fsFile.buildFullName();
        byte[] inputData = new byte[10000];

        // read base content
        int bytesNumber = fs.readFile(fileName, 0, inputData);
        Assert.assertEquals(fileContent.getBytes().length, bytesNumber);
        String checkContent = new String(Arrays.copyOf(inputData, bytesNumber));
        String content2 = FsUtils.readFileToString(fs,fileName);

        //log.info("fileContent size " + fileContent.length() + ", [" + fileContent + "]");
        //log.info("checkContent size " + checkContent.length() + ", [" + checkContent + "]");
        //log.info("content2 size " + content2.length() + ", [" + content2 + "]");

        Assert.assertEquals(fileContent, checkContent);
        Assert.assertEquals(fileContent, content2);

    }
    protected static List<FsFileInfo> fsToLog(FsInMemory fs) {
        List<FsFileInfo> fsContent = FsUtils.scanAllContent(fs,FsInMemory.ROOT_FOLDER_NAME);
        //Collections.sort(fsContent);
        StringBuilder sb = new StringBuilder("fs content is:\n");
        for (FsFileInfo fi : fsContent ) {
            sb.append(String.format("%s, size=%d name=%s \n",fi.isFolder()?"FOLDER" : "FILE", fi.getSize(), fi.getPath().buildFullName() ));
        }
        log.info (sb.toString());
        return fsContent;
    }

}
