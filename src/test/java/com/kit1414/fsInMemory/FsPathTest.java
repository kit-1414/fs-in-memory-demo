package com.kit1414.fsInMemory;

import com.kit1414.fsInMemory.impl.FsPath;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by kit_1414 on 03-Dec-16.
 */
public class FsPathTest {

    @Test
    public void testBuildPath() {
        FsPath path = new FsPath();
        FsPath path1 = new FsPath();

        Assert.assertTrue(path.isRoot());
        Assert.assertEquals(0,path.getSize());
        Assert.assertEquals("/",path.buildFullName());

        final String name = "ssss";

        path.addOne(name);
        path1.addOne(name);

        Assert.assertEquals("/"+name,path.buildFullName());
        Assert.assertTrue(path.equals(path1));
        Assert.assertFalse(path.isRoot());
        Assert.assertEquals(1,path.getSize());

        final String name1 = "ssssa";

        path.addOne(name);
        path1.addOne(name1);

        Assert.assertEquals("/"+name+"/" + name, path.buildFullName());
        Assert.assertEquals("/"+name+"/" + name1, path1.buildFullName());

        Assert.assertFalse(path.equals(path1));
        Assert.assertFalse(path.isRoot());
        Assert.assertEquals(2,path.getSize());
    }
    @Test
    public void testCheckPath() {
        FsPath pathRoot = new FsPath("/");
        Assert.assertTrue(pathRoot.isRoot());
        Assert.assertNull(pathRoot.getParent());
        Assert.assertEquals("/", pathRoot.buildFullName());

        Assert.assertNull(pathRoot.getParent());

        pathRoot = new FsPath("/aaaa");
        Assert.assertFalse(pathRoot.isRoot());
        Assert.assertEquals(1, pathRoot.getSize());
        Assert.assertEquals("/aaaa", pathRoot.buildFullName());

        {
            FsPath parent = pathRoot.getParent();
            Assert.assertEquals(0, parent.getSize());
            Assert.assertEquals("/", parent.buildFullName());
        }

        pathRoot = new FsPath("/aaaa/ddddd1234");
        Assert.assertFalse(pathRoot.isRoot());
        Assert.assertEquals(2, pathRoot.getSize());
        Assert.assertEquals("/aaaa/ddddd1234", pathRoot.buildFullName());
        {
            FsPath parent = pathRoot.getParent();
            Assert.assertEquals(1, parent.getSize());
            Assert.assertEquals("/aaaa", parent.buildFullName());
        }


        pathRoot = new FsPath("/aaaa/ddddd1234/e3qweq");
        Assert.assertFalse(pathRoot.isRoot());
        Assert.assertEquals(3, pathRoot.getSize());
        Assert.assertEquals("/aaaa/ddddd1234/e3qweq", pathRoot.buildFullName());

        {
            FsPath parent = pathRoot.getParent();
            Assert.assertEquals(2, parent.getSize());
            Assert.assertEquals("/aaaa/ddddd1234", parent.buildFullName());
        }

    }
}

