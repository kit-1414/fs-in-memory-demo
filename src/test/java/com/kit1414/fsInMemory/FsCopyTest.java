package com.kit1414.fsInMemory;

import com.kit1414.fsInMemory.exception.FsDuplicateNameException;
import com.kit1414.fsInMemory.exception.FsException;
import com.kit1414.fsInMemory.impl.FsPath;
import com.kit1414.fsInMemory.impl.FsUtils;
import org.junit.Before;
import org.junit.Test;
import org.testng.Assert;

/**
 * Created by kit_1414 on 03-Dec-16.
 */
public class FsCopyTest extends BaseFsTest {
    @Before
    public void setUp() {
        super.setUp();
    }

    @Test
    public void copyFolderTest() {

        final String folderConst = "/base1/base2/mmm33/const/11";

        final String folderOne =    "/base1/base2/mmm33/toCopy/55/77";
        final String fileBefore =   "/base1/base2/mmm33/toCopy/55.log";
        final String folderToCopy = "/base1/base2/mmm33/toCopy";
        final String fileContentOld = "1234e2423eweddas";
        final String fileContentNew = "dddddddddddddddd";


        final String destFolder =  "/base1/base2/destFolder";
        final String folderConst2=   "/base1/base2/destFolder/aaaaa";

        final String fileAfter = "/base1/base2/destFolder/toCopy/55.log";
        final String folderAfter = "/base1/base2/destFolder/toCopy/55/77";

        Assert.assertEquals(FsUtils.checkCreateFolder(fs, new FsPath(folderConst)),folderConst);
        Assert.assertEquals(FsUtils.checkCreateFolder(fs, new FsPath(folderOne)),folderOne);
        Assert.assertEquals(FsUtils.checkCreateFolder(fs, new FsPath(folderConst2)),folderConst2);
        Assert.assertEquals(fs.createFile(fileBefore,fileContentOld.getBytes()),fileBefore);


        Assert.assertTrue(fs.isExists(folderOne));
        Assert.assertTrue(fs.isFolder(folderOne));
        Assert.assertEquals(fs.getSize(folderOne),0);

        Assert.assertTrue(fs.isExists(folderConst));
        Assert.assertTrue(fs.isFolder(folderConst));

        Assert.assertTrue(fs.isExists(folderConst2));
        Assert.assertTrue(fs.isFolder(folderConst2));

        Assert.assertTrue(fs.isExists(fileBefore));
        Assert.assertTrue(fs.isFile(fileBefore));

        // copy ...
        String resultFolder = fs.copy(folderToCopy,destFolder);
        Assert.assertEquals(resultFolder,destFolder + "/toCopy");

        Assert.assertTrue(fs.isExists(folderConst));
        Assert.assertTrue(fs.isFolder(folderConst));

        Assert.assertTrue(fs.isExists(folderConst2));
        Assert.assertTrue(fs.isFolder(folderConst2));

        Assert.assertTrue(fs.isExists(folderOne));
        Assert.assertTrue(fs.isExists(fileBefore));

        Assert.assertTrue(fs.isExists(fileAfter));
        Assert.assertTrue(fs.isFile(fileAfter));

        Assert.assertTrue(fs.isExists(folderAfter));
        Assert.assertTrue(fs.isFolder(folderAfter));

        Assert.assertEquals(FsUtils.readFileToString(fs,fileAfter),fileContentOld);
        // check, that change on one doesn't affect another
        fs.editFile(fileAfter,0,fileContentNew.getBytes());
        Assert.assertEquals(FsUtils.readFileToString(fs,fileAfter),fileContentNew);
        Assert.assertEquals(FsUtils.readFileToString(fs,fileBefore),fileContentOld);

        Assert.assertEquals(fs.getSize(folderOne),0);
        Assert.assertEquals(fs.getSize(folderAfter),0);

        fs.createFolder(folderAfter + "/newFolder");

        Assert.assertEquals(fs.getSize(folderOne),0);
        Assert.assertEquals(fs.getSize(folderAfter),1);


    }

    @Test
    public void copyFileTest() {

        final String folderConst =  "/base1/base2/mmm33/const/11";
        final String fileToCopy = "/base1/base2/mmm33/toCopy.log";

        final String fileContentOld = "1234e2423eweddas";
        final String fileContentNew = "dddddddddddddddd";


        final String destFolder =  "/base1/base2/destFolder";
        final String folderDestConst2=   "/base1/base2/destFolder/aaaaa";

        final String fileAfter = "/base1/base2/destFolder/toCopy.log";

        Assert.assertEquals(FsUtils.checkCreateFolder(fs, new FsPath(folderConst)),folderConst);
        Assert.assertEquals(FsUtils.checkCreateFolder(fs, new FsPath(folderDestConst2)),folderDestConst2);
        Assert.assertEquals(fs.createFile(fileToCopy,fileContentOld.getBytes()),fileToCopy);

        Assert.assertTrue(fs.isExists(folderConst));
        Assert.assertTrue(fs.isFolder(folderConst));

        Assert.assertTrue(fs.isExists(folderDestConst2));
        Assert.assertTrue(fs.isFolder(folderDestConst2));

        Assert.assertTrue(fs.isExists(fileToCopy));
        Assert.assertTrue(fs.isFile(fileToCopy));

        // Copy ...
        String resultFile = fs.copy(fileToCopy,destFolder);
        Assert.assertEquals(resultFile,fileAfter);

        Assert.assertTrue(fs.isExists(folderConst));
        Assert.assertTrue(fs.isFolder(folderConst));

        Assert.assertTrue(fs.isExists(folderDestConst2));
        Assert.assertTrue(fs.isFolder(folderDestConst2));


        Assert.assertTrue(fs.isExists(fileAfter));
        Assert.assertTrue(fs.isFile(fileAfter));

        Assert.assertEquals(FsUtils.readFileToString(fs,fileAfter),fileContentOld);

        // check, that change on one doesn't affect another
        fs.editFile(fileAfter,0,fileContentNew.getBytes());
        Assert.assertEquals(FsUtils.readFileToString(fs,fileAfter),fileContentNew);
        Assert.assertEquals(FsUtils.readFileToString(fs,fileToCopy),fileContentOld);

    }

    @Test(expected = FsException.class)
    public void copyFileToSelfTest() {
        final String fileToCopy = "/base1/base2/mmm33/toCopy.log";
        final String destFolder = "/base1/base2/mmm33";
        FsUtils.checkCreateFile(fs, new FsPath(fileToCopy),"".getBytes());
        fs.copy(fileToCopy, destFolder);
    }

    @Test(expected = FsException.class)
    public void copyFolderToSelfTest() {
        final String folderOne =    "/base1/base2/mmm33/toCopy/ff/gg";
        final String folderToCopy = "/base1/base2/mmm33/toCopy";
        final String destFolder =   "/base1/base2/mmm33";
        FsUtils.checkCreateFolder(fs, new FsPath(folderOne));
        fs.copy(folderToCopy, destFolder);
    }

    @Test(expected = FsDuplicateNameException.class)
    public void copyFileToFolder() {
        // there is a folder in dest folder with same name as new file
        final String folderOne =    "/base1/base2/mmm33/destFolder/ff/gg";
        final String folderTwo =    "/base1/base2/mmm33/destFolder/toCopy.log/fff";
        final String fileToCopy = "/base1/base2/mmm33/toCopy.log"; // there is a folder this target name
        final String destFolder =   "/base1/base2/mmm33/destFolder";

        try {
            FsUtils.checkCreateFolder(fs, new FsPath(folderOne));
            FsUtils.checkCreateFolder(fs, new FsPath(folderTwo));
            fs.createFile(fileToCopy, "".getBytes());
        } catch (FsException ex) {
            Assert.fail(" must not happen here - " + ex);
        }

        fs.copy(fileToCopy, destFolder);
    }
    @Test(expected = FsDuplicateNameException.class)
    public void copyFolderToFile() {
        // there is a file in dest folder with same name as new folder
        final String folderOne =    "/base1/base2/mmm33/toCopy/ff/gg";
        final String folderToCopy = "/base1/base2/mmm33/toCopy";

        final String folderTwo =    "/base1/base2/mmm33/destFolder/toCopy";
        final String problemFile =  "/base1/base2/mmm33/destFolder/toCopy/ff";
        final String destFolder =   "/base1/base2/mmm33/destFolder";

        try {
            FsUtils.checkCreateFolder(fs, new FsPath(folderOne));
            FsUtils.checkCreateFolder(fs, new FsPath(folderTwo));
            fsToLog(fs);
            fs.createFile(problemFile,"".getBytes());
        } catch (FsException ex) {
            Assert.fail(" must not happen here - " + ex);
        }
        fs.copy(folderToCopy, destFolder);
    }
}
