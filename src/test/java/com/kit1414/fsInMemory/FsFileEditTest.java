package com.kit1414.fsInMemory;

import com.kit1414.fsInMemory.impl.FsUtils;
import org.junit.Before;
import org.testng.Assert;
import org.testng.annotations.Test;


/**
 * Created by kit_1414 on 03-Dec-16.
 */

public class FsFileEditTest extends BaseFsTest {

    @Before
    public void setUp() {
        super.setUp();
    }

    @Test
    public void fsRootCreationReadTest() {
        final String file = "/fileNNN.data";
        final String baseFileData = "0123456789";
        final String delta = "ab";
        final int offset = 2;
        final String newFileData = "01ab456789";

        // create file
        fs.createFile(file, baseFileData.getBytes());

        // read file, check content
        String checkS = FsUtils.readFileToString(fs, file);
        Assert.assertEquals(checkS , baseFileData);

        // edit file, check new content
        fs.editFile(file, offset, delta.getBytes());
        checkS = FsUtils.readFileToString(fs, file);
        Assert.assertEquals(checkS , newFileData);

    }
}
