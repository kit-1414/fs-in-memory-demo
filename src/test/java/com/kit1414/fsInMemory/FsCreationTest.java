package com.kit1414.fsInMemory;

import com.kit1414.fsInMemory.exception.FsException;
import com.kit1414.fsInMemory.exception.FsFileNotFoundException;
import com.kit1414.fsInMemory.exception.FsInvalidPathException;
import com.kit1414.fsInMemory.impl.FsInMemoryImpl;
import com.kit1414.fsInMemory.impl.FsPath;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


public class FsCreationTest extends BaseFsTest {

    @Before
    public void setUp() {
        super.setUp();
    }


    @Test(expected = FsInvalidPathException.class)
    public void creationRootFolderTest() {
        fs.createFolder("/");
    }

    @Test(expected = FsInvalidPathException.class)
    public void creationRootFileTest() {
        fs.createFile("/", "".getBytes());
    }

    @Test
    public void creationReadTest() {
        final String folder1 = "/folder1";
        final String folder2 = "/folder2";
        final String file1 = "/file1";
        final String file2 = "/file2";
        final String fileContentS = "file 1 content";


        {
            FsPath rootFolder = new FsPath();
            FsPath fsFolder1 = new FsPath(folder1);
            FsPath fsFolder2 = new FsPath(folder2);
            FsPath fsFile1 = new FsPath(file1);
            FsPath fsFile2 = new FsPath(file2);
            fsRootCreationReadTest(fs, rootFolder, fsFolder1, fsFolder2, fsFile1, fsFile2, fileContentS);
            // first 4 folders created
        }
        {
            String foldert44 = "/folder44";
            fs.createFolder(foldert44);
            // 4 + 1 folder
            FsPath rootFolder = new FsPath(foldert44);
            FsPath fsFolder1 = new FsPath(foldert44 + folder1);
            FsPath fsFolder2 = new FsPath(foldert44 + folder2);
            FsPath fsFile1 = new FsPath(foldert44 + file1);
            FsPath fsFile2 = new FsPath(foldert44 + file2);
            // more 4 folders
            fsRootCreationReadTest(fs, rootFolder, fsFolder1, fsFolder2, fsFile1, fsFile2, fileContentS);
        }
        int filesNumber = 4 + 1 + 4 + 1; // last 1 - self
        Assert.assertEquals(filesNumber,fsToLog(fs).size());
    }

    protected static void fsRootCreationReadTest(FsInMemory fs, FsPath fsRoot, FsPath fsFolder1, FsPath fsFolder2, FsPath fsFile1, FsPath fsFile2, String file1Content) {
        final String rootFolder = fsRoot.buildFullName();
        final String folder1 = fsFolder1.buildFullName();
        final String folder2 = fsFolder2.buildFullName();

        final String file1 = fsFile1.buildFullName();
        final String file2 = fsFile2.buildFullName();

        // check base folder empty
        Assert.assertEquals(0, fs.readFolder(rootFolder).size());

        checkCreateCreateFolder(fs,fsRoot,fsFolder1);
        // check base folder has 1 subitem
        Assert.assertEquals(1, fs.readFolder(rootFolder).size());

        checkCreateCreateFolder(fs,fsRoot,fsFolder2);
        Assert.assertEquals(2, fs.readFolder(rootFolder).size());

        checkCreateCreateFile(fs,fsRoot,fsFile1, file1Content);
        Assert.assertEquals(3, fs.readFolder(rootFolder).size());

        checkCreateCreateFile(fs,fsRoot,fsFile2, file1Content);
        Assert.assertEquals(4, fs.readFolder(rootFolder).size());
        //--------------------
        Set<String> testContent = new HashSet<>(Arrays.asList(folder1, folder2, file1, file2));
        Set<String> folderContent = fs.readFolder(rootFolder);

        List<String> fullNames = folderContent.stream().map(it -> ( FsPath.createNew(rootFolder).addOne(it).buildFullName())).collect(Collectors.toList());
        Assert.assertEquals(4, fullNames.size());
        testContent.removeAll(fullNames);
        Assert.assertEquals(0, testContent .size());
    }
    @Test(expected = FsException.class)
    public void creationWrongFolder1() {
        fs.createFolder("/aa//");
    }
    @Test(expected = FsException.class)
    public void creationWrongFolder2() {
        fs.createFolder("/aa/bbb/");
    }
    @Test(expected = FsException.class)
    public void creationWrongFolder3() {
        fs.createFolder("/aa/bbb/c&");
    }
    @Test(expected = FsException.class)
    public void creationWrongFile1() {
        fs.createFolder("/aa//zz.log");
    }
    @Test(expected = FsException.class)
    public void creationWrongFile2() {
        fs.createFolder("/aa/z*z.log");
    }
}

