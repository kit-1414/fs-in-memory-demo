package com.kit1414.fsInMemory;

import com.kit1414.fsInMemory.exception.FsDuplicateNameException;
import com.kit1414.fsInMemory.exception.FsException;
import com.kit1414.fsInMemory.impl.FsPath;
import com.kit1414.fsInMemory.impl.FsUtils;
import org.junit.Before;
import org.junit.Test;
import org.testng.Assert;

/**
 * Created by kit_1414 on 03-Dec-16.
 */
public class FsMoveTest extends BaseFsTest  {

    @Before
    public void setUp() {
        super.setUp();
    }

    @Test
    public void moveFolderTest() {

        final String folderConst = "/base1/base2/mmm33/const/11";

        final String folderOne =    "/base1/base2/mmm33/toMove/55/77";
        final String fileBefore =   "/base1/base2/mmm33/toMove/55.log";
        final String folderToMove = "/base1/base2/mmm33/toMove";
        final String fileContent = "1234e2423eweddas";


        final String destFolder =  "/base1/base2/destFolder";
        final String folderConst2=   "/base1/base2/destFolder/aaaaa";

        final String fileAfter = "/base1/base2/destFolder/toMove/55.log";
        final String folderAfter = "/base1/base2/destFolder/toMove/55/77";

        Assert.assertEquals(FsUtils.checkCreateFolder(fs, new FsPath(folderConst)),folderConst);
        Assert.assertEquals(FsUtils.checkCreateFolder(fs, new FsPath(folderOne)),folderOne);
        Assert.assertEquals(FsUtils.checkCreateFolder(fs, new FsPath(folderConst2)),folderConst2);
        Assert.assertEquals(fs.createFile(fileBefore,fileContent.getBytes()),fileBefore);


        Assert.assertTrue(fs.isExists(folderOne));
        Assert.assertTrue(fs.isFolder(folderOne));

        Assert.assertTrue(fs.isExists(folderConst));
        Assert.assertTrue(fs.isFolder(folderConst));

        Assert.assertTrue(fs.isExists(folderConst2));
        Assert.assertTrue(fs.isFolder(folderConst2));

        Assert.assertTrue(fs.isExists(fileBefore));
        Assert.assertTrue(fs.isFile(fileBefore));

        // Move ...
        String resultFolder = fs.move(folderToMove,destFolder);
        Assert.assertEquals(resultFolder,destFolder + "/toMove");

        Assert.assertTrue(fs.isExists(folderConst));
        Assert.assertTrue(fs.isFolder(folderConst));

        Assert.assertTrue(fs.isExists(folderConst2));
        Assert.assertTrue(fs.isFolder(folderConst2));

        Assert.assertFalse(fs.isExists(folderOne));
        Assert.assertFalse(fs.isExists(fileBefore));

        Assert.assertTrue(fs.isExists(fileAfter));
        Assert.assertTrue(fs.isFile(fileAfter));

        Assert.assertTrue(fs.isExists(folderAfter));
        Assert.assertTrue(fs.isFolder(folderAfter));

        Assert.assertEquals(FsUtils.readFileToString(fs,fileAfter),fileContent);

    }
    @Test
    public void moveFileTest() {

        final String folderConst =  "/base1/base2/mmm33/const/11";
        final String fileToMove = "/base1/base2/mmm33/toMove.log";

        final String fileContent = "1234e2423eweddas";


        final String destFolder =  "/base1/base2/destFolder";
        final String folderConst2=   "/base1/base2/destFolder/aaaaa";

        final String fileAfter = "/base1/base2/destFolder/toMove.log";

        Assert.assertEquals(FsUtils.checkCreateFolder(fs, new FsPath(folderConst)),folderConst);
        Assert.assertEquals(FsUtils.checkCreateFolder(fs, new FsPath(folderConst2)),folderConst2);
        Assert.assertEquals(fs.createFile(fileToMove,fileContent.getBytes()),fileToMove);

        Assert.assertTrue(fs.isExists(folderConst));
        Assert.assertTrue(fs.isFolder(folderConst));

        Assert.assertTrue(fs.isExists(folderConst2));
        Assert.assertTrue(fs.isFolder(folderConst2));

        Assert.assertTrue(fs.isExists(fileToMove));
        Assert.assertTrue(fs.isFile(fileToMove));

        // Move ...
        String resultFile = fs.move(fileToMove,destFolder);
        Assert.assertEquals(resultFile,fileAfter);

        Assert.assertTrue(fs.isExists(folderConst));
        Assert.assertTrue(fs.isFolder(folderConst));

        Assert.assertTrue(fs.isExists(folderConst2));
        Assert.assertTrue(fs.isFolder(folderConst2));


        Assert.assertTrue(fs.isExists(fileAfter));
        Assert.assertTrue(fs.isFile(fileAfter));

        Assert.assertEquals(FsUtils.readFileToString(fs,fileAfter),fileContent);

    }
    @Test(expected = FsException.class)
    public void copyFileToSelfTest() {
        final String fileToCopy = "/base1/base2/mmm33/toCopy.log";
        final String destFolder = "/base1/base2/mmm33";
        FsUtils.checkCreateFile(fs, new FsPath(fileToCopy),"".getBytes());
        fs.move(fileToCopy, destFolder);
    }

    @Test(expected = FsException.class)
    public void copyFolderToSelfTest() {
        final String folderOne = "/base1/base2/mmm33/toCopy/ff/gg";
        final String folderToCopy = "/base1/base2/mmm33/toCopy";
        final String destFolder = "/base1/base2/mmm33";
        FsUtils.checkCreateFolder(fs, new FsPath(folderOne));
        fs.move(folderToCopy, destFolder);
    }
    @Test(expected = FsDuplicateNameException.class)
    public void moveFileToFolder() {
        // there is a folder in dest folder with same name as new file
        final String folderOne =    "/base1/base2/mmm33/destFolder/ff/gg";
        final String folderTwo =    "/base1/base2/mmm33/destFolder/toCopy.log/fff";
        final String fileToCopy = "/base1/base2/mmm33/toCopy.log"; // there is a folder this target name
        final String destFolder =   "/base1/base2/mmm33/destFolder";

        try {
            FsUtils.checkCreateFolder(fs, new FsPath(folderOne));
            FsUtils.checkCreateFolder(fs, new FsPath(folderTwo));
            fs.createFile(fileToCopy, "".getBytes());
        } catch (FsException ex) {
            Assert.fail(" must not happen here - " + ex);
        }

        fs.move(fileToCopy, destFolder);
    }
    @Test(expected = FsDuplicateNameException.class)
    public void moveFolderToFile() {
        // there is a file in dest folder with same name as new folder
        final String folderOne =    "/base1/base2/mmm33/toCopy/ff/gg";
        final String folderToCopy = "/base1/base2/mmm33/toCopy";

        final String folderTwo =    "/base1/base2/mmm33/destFolder/toCopy";
        final String problemFile =  "/base1/base2/mmm33/destFolder/toCopy/ff";
        final String destFolder =   "/base1/base2/mmm33/destFolder";

        try {
            FsUtils.checkCreateFolder(fs, new FsPath(folderOne));
            FsUtils.checkCreateFolder(fs, new FsPath(folderTwo));
            fsToLog(fs);
            fs.createFile(problemFile,"".getBytes());
        } catch (FsException ex) {
            Assert.fail(" must not happen here - " + ex);
        }
        fs.move(folderToCopy, destFolder);
    }
}
