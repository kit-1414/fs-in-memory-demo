package com.kit1414.fsInMemory;

import com.kit1414.fsInMemory.exception.FsException;
import com.kit1414.fsInMemory.exception.FsFileNotFoundException;
import com.kit1414.fsInMemory.exception.FsInvalidPathException;
import com.kit1414.fsInMemory.impl.FsPath;
import com.kit1414.fsInMemory.impl.FsUtils;
import org.junit.Before;
import org.junit.Test;
import org.testng.Assert;

/**
 * Created by kit_1414 on 03-Dec-16.
 */
public class FsDeleteTest extends BaseFsTest  {

    @Before
    public void setUp() {
        super.setUp();
    }

    @Test(expected = FsException.class)
    public void deleteRootTest() {
        fs.delete("/");
    }

    @Test
    public void deleteTest() {

        final String baseFolder1 = "/";
        final String baseFolder2 = "/base1/base2";
        FsPath fsBaseFolder1  = new FsPath(baseFolder1);
        FsPath fsBaseFolder2  = new FsPath(baseFolder2);


        Assert.assertEquals(0, fs.getSize("/"));
        deleteInFolderTest(fs,fsBaseFolder1);
        Assert.assertEquals(0, fs.getSize("/"));

        Assert.assertEquals(baseFolder2, FsUtils.checkCreateFolder(fs, fsBaseFolder2));
        Assert.assertTrue(fs.isFolder(baseFolder2));
        Assert.assertEquals(1, fs.getSize("/"));

        deleteInFolderTest(fs,fsBaseFolder2);
        Assert.assertEquals(1, fs.getSize("/"));

        fs.delete(fsBaseFolder2.buildFullName()); // remove last folder
        Assert.assertEquals(1, fs.getSize("/"));
        Assert.assertEquals(0, fs.getSize(fsBaseFolder2.getParent().buildFullName()));

        fs.delete(fsBaseFolder2.getParent().buildFullName()); // remove parent of folder2
        Assert.assertEquals(0, fs.getSize("/"));
    }
    private static void deleteInFolderTest(FsInMemory fs, FsPath baseFolder) {
        final String fileContentS = "file 1 content";
        final String baseFolderFullName = baseFolder.buildFullName();

        FsPath fsFolder1  = new FsPath(baseFolder).addOne("folder1");
        String folder1FullName = fsFolder1.buildFullName();
        Assert.assertEquals(folder1FullName, FsUtils.checkCreateFolder(fs, fsFolder1));


        FsPath fsFolder2  = new FsPath(baseFolder).addOne("folder2").addOne("folder33");
        String folder2FullName =  fsFolder2.buildFullName();
        Assert.assertEquals(folder2FullName, FsUtils.checkCreateFolder(fs, fsFolder2));

        FsPath fsFile1  = new FsPath(baseFolder).addOne("file1");
        String file1FullName = fsFile1.buildFullName();
        Assert.assertEquals(file1FullName , FsUtils.checkCreateFile(fs, fsFile1, fileContentS.getBytes()));

        FsPath fsFile2  = new FsPath(baseFolder).addOne("file2");
        String fsFile2FullName = fsFile2.buildFullName();
        Assert.assertEquals(fsFile2FullName , FsUtils.checkCreateFile(fs, fsFile2, fileContentS.getBytes()));

        Assert.assertEquals(fs.getSize(baseFolderFullName),4);

        deleteFolderTest(fs,baseFolder,fsFolder1);
        Assert.assertEquals(fs.getSize(baseFolderFullName),3);


        deleteFolderTest(fs,baseFolder,fsFolder2.getParent()); // there are 2 subfolders here, so it's require to remove base
        Assert.assertEquals(fs.getSize(baseFolderFullName),2);

        deleteFileTest(fs,baseFolder,fsFile1);
        Assert.assertEquals(fs.getSize(baseFolderFullName),1);

        deleteFileTest(fs,baseFolder,fsFile2);
        Assert.assertEquals(fs.getSize(baseFolderFullName),0);

    }
    private static void deleteFolderTest(FsInMemory fs, FsPath baseFolder, FsPath folderToDelete) {
        String folderFullName = folderToDelete.buildFullName();
        Assert.assertEquals(folderFullName, FsUtils.checkCreateFolder(fs, folderToDelete));
        Assert.assertTrue(fs.isExists(folderFullName));
        Assert.assertTrue(fs.isFolder(folderFullName));
        Assert.assertFalse(fs.isFile(folderFullName));

        fs.delete(folderFullName);
        Assert.assertFalse(fs.isExists(folderFullName));
        Assert.assertTrue(fs.isExists(baseFolder.buildFullName()));

        boolean exceptionHappen = false;
        try {
            fs.isFolder(folderFullName);
            Assert.fail("Exception mut happen");
        } catch (FsFileNotFoundException ex) {
            exceptionHappen = true;
        }
        Assert.assertTrue(exceptionHappen);

        exceptionHappen = false;
        try {
            fs.isFile(folderFullName);
            Assert.fail("Exception mut happen");
        } catch (FsFileNotFoundException ex) {
            exceptionHappen = true;
        }
        Assert.assertTrue(exceptionHappen);

        exceptionHappen = false;
        try {
            fs.getSize(folderFullName);
            Assert.fail("Exception mut happen");
        } catch (FsFileNotFoundException ex) {
            exceptionHappen = true;
        }
        Assert.assertTrue(exceptionHappen);
    }
    private static void deleteFileTest(FsInMemory fs, FsPath baseFolder, FsPath fileToDelete) {
        String fileFullName = fileToDelete.buildFullName();
        Assert.assertFalse(fs.isFolder(fileFullName));
        Assert.assertTrue(fs.isFile(fileFullName));
        Assert.assertTrue(fs.isExists(fileFullName));

        fs.delete(fileFullName);
        Assert.assertFalse(fs.isExists(fileFullName));
        Assert.assertTrue(fs.isExists(baseFolder.buildFullName()));

        boolean exceptionHappen = false;
        try {
            fs.isFolder(fileFullName);
            Assert.fail("Exception mut happen");
        } catch (FsFileNotFoundException ex) {
            exceptionHappen = true;
        }
        Assert.assertTrue(exceptionHappen);

        exceptionHappen = false;
        try {
            fs.isFile(fileFullName);
            Assert.fail("Exception mut happen");
        } catch (FsFileNotFoundException ex) {
            exceptionHappen = true;
        }
        Assert.assertTrue(exceptionHappen);

        exceptionHappen = false;
        try {
            fs.getSize(fileFullName);
            Assert.fail("Exception mut happen");
        } catch (FsFileNotFoundException ex) {
            exceptionHappen = true;
        }
        Assert.assertTrue(exceptionHappen);
    }
}
